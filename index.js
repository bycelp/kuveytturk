import React, { Component } from 'react';
import { AppRegistry, Text, Image, TouchableOpacity,View} from 'react-native';
console.disableYellowBox = true;
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import Anasayfa from './src/screen/Anasayfa';
import Fatura from './src/screen/Fatura';
import GiderKontrol from './src/screen/GiderKontrol';
import Personeller from './src/screen/Personeller';
import Popup from './src/screen/Popup';
import Popup2 from './src/screen/Popup2';
import Loading from './src/screen/Loading';
import EFatura from './src/screen/E-Fatura';
import Tablet from './src/screen/Tablet';
import MusteriDestek from './src/screen/MusteriDestek';


const PTP = (SomeComponent) => {
  return class extends Component {
      static navigationOptions = SomeComponent.navigationOptions; // better use hoist-non-react-statics
      render() {
          const {navigation: {state: {params}}} = this.props
          return <SomeComponent {...params} {...this.props} />
      }
  }
}

const routeConfigs = {
  Loading: {
    screen: Loading,
    navigationOptions: {
      header: null,
    }
  },
  Anasayfa: {
    screen: Anasayfa,
    navigationOptions: {
      header: null,
    }
  },
  Fatura: {
    screen: Fatura,
    navigationOptions: ({navigation}) => ({
      headerTitle: `Faturalar`,
      headerStyle: {
        height : 60,
        backgroundColor: '#054f3e',
        borderBottomWidth: 0,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          textAlign: "center",
          alignSelf: 'center',
          color: "#fff",
          fontSize : 20,
      },
    })
  },
  EFatura: {
    screen: EFatura,
    navigationOptions: ({navigation}) => ({
      headerTitle: `E-Faturanız`,
      headerStyle: {
        height : 60,
        backgroundColor: '#054f3e',
        borderBottomWidth: 0,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          textAlign: "center",
          alignSelf: 'center',
          color: "#fff",
          fontSize : 20,
      },
    })
  },
  GiderKontrol: {
    screen: GiderKontrol,
    navigationOptions: ({navigation}) => ({
      headerTitle: `Gider Kontrol`,
      headerStyle: {
        height : 60,
        backgroundColor: '#054f3e',
        borderBottomWidth: 0,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          textAlign: "center",
          alignSelf: 'center',
          color: "#fff",
          fontSize : 20,
      },
    })
  },
  Personeller: {
    screen: Personeller,
    navigationOptions: ({navigation}) => ({
      headerTitle: `Gider Kontrol`,
      headerStyle: {
        height : 60,
        backgroundColor: '#054f3e',
        borderBottomWidth: 0,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          textAlign: "center",
          alignSelf: 'center',
          color: "#fff",
          fontSize : 20,
      },
    })
  },
  Popup: {
    screen: PTP(Popup),
    navigationOptions: ({navigation}) => ({
      headerTitle: 'Dışarı Aktar',
      headerStyle: {
        height : 60,
        backgroundColor: '#054f3e',
        borderBottomWidth: 0,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          textAlign: "center",
          alignSelf: 'center',
          color: "#fff",
          fontSize : 20,
      },
    })
  },
  Popup2: {
    screen: Popup2,
    navigationOptions: {
      header: null,
    }
  },
  Tablet: {
    screen: Tablet,
    navigationOptions: {
      header: null,
    }
  },
  MusteriDestek: {
    screen: MusteriDestek,
    navigationOptions: ({navigation}) => ({
      headerTitle: `Müşteri Destek`,
      headerStyle: {
        height : 60,
        backgroundColor: '#054f3e',
        borderBottomWidth: 0,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
          fontWeight: "normal",
          fontStyle: "normal",
          letterSpacing: 0,
          textAlign: "center",
          alignSelf: 'center',
          color: "#fff",
          fontSize : 20,
      },
    })
  },

}

AppRegistry.registerComponent('KuveytTurk', () => createStackNavigator(routeConfigs));