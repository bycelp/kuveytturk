import {observable, action} from 'mobx'

class Store{
    @observable Account = {}

    @action setAccount(veri) {
        this.Account = veri;
    }
}

export default new Store()