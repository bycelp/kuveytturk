import React, { Component } from 'react';
import { View, Text,Image, ScrollView, TouchableOpacity, Alert } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Store from '../mobx/Store';
import {observer} from 'mobx-react';
import Icon from 'react-native-vector-icons/FontAwesome5';

@observer
export default class Fatura extends Component {
  constructor(props) {
    super(props);
    this.state = {
        modal: true
    };
  }

  resmilestir(){
      this.props.navigation.navigate('EFatura');
  }

  faturaAlert(){
    Alert.alert(
        'Merhaba!',
        'Lütfen yapmak istediğiniz işlemi seçiniz!',
        [
          {text: 'Dekont Göster', onPress: () => this.dekont()},
          {text: 'Geliri Resmileştir', onPress: () => this.resmilestir()},
        ],
        {cancelable: true},
      );
  }

  render() {
    return (
     <View style={styles.container}>
     <ScrollView>
     <View style={styles.hesapDurumu}>

     <View style={styles.hesapDurumuTarihView}>
     <Text style={styles.hesapDurumuText}>Hesap Durumu</Text>
     <Text style={styles.tarihText}>05-12 Nisan 2019</Text>
     </View>


     <View style={styles.buHaftaView}>
     <Text style={styles.hesapDurumuText}>Bu Hafta</Text>
     <Icon name="chevron-down" size={15} color={'black'}  />
     </View>

     </View>

        

        <View style={styles.bakiye}>

        <View>
        <Text style={styles.bakiyeText}>{(Store.Account.availableBalance/2400).toFixed(2)} TL</Text>
        <View style={styles.cizgi1}></View>
        <Text style={styles.gelenBakiyeText}>Gelen Bakiye</Text>
        </View>

        <View >
        <Text style={styles.bakiyeText2}>{(Store.Account.availableBalance/3200).toFixed(2)} TL</Text>
        <View style={styles.cizgi2}></View>
        <Text style={styles.gidenBakiyeText}>Giden Bakiye</Text>
        </View>
        
        </View>


        <View style={styles.sonHareketlerView}>
        <Text style={styles.sonHareketlerText}>Son Hareketler</Text>
        </View>


        <View style={styles.infoView}>
        <View style={styles.infovelogo}>
        <Image  resizeMode='contain' source={require('../image/imageServiceMobile.png' )}style={{width: wp('15%'), height : wp('15%'),  }}  />
        
        <View style={styles.textView}>
        <Text style={styles.tarihText}>13 Nisan 2019</Text>
        <Text style={styles.akaryakitText}>Mobil Uygulama Hizmeti</Text>
        </View>
        </View>

        <View style={{flexDirection: 'row'}}>
        <Text style={[styles.gelenBakiyeText, { marginTop: 5 }]}>+3000,00 TL</Text>
        <TouchableOpacity onPress={() => this.faturaAlert() }>
        <Icon name="file-invoice" size={25} color={'#9f9f9f'} style={{paddingLeft : 5}}/>
        </TouchableOpacity>
        </View>
        </View>

        <View style={styles.infoView}>
        <View style={styles.infovelogo}>
        <Image  resizeMode='contain' source={require('../image/eat.png' )}style={{width: wp('15%'), height : wp('15%'),  }}  />
        
        <View style={styles.textView}>
        <Text style={styles.tarihText}>07 Nisan 2019</Text>
        <Text style={styles.akaryakitText}>Yemek Limiti Yüklemesi</Text>
        </View>
        </View>

        <Text style={styles.gelenBakiyeText}>-140,00 TL</Text>
        </View>

        <View style={styles.infoView}>
        <View style={styles.infovelogo}>
        <Image  resizeMode='contain' source={require('../image/imageServiceMobile.png' )}style={{width: wp('15%'), height : wp('15%'),  }}  />
        
        <View style={styles.textView}>
        <Text style={styles.tarihText}>13 Nisan 2019</Text>
        <Text style={styles.akaryakitText}>Mobil Uygulama Hizmeti</Text>
        </View>
        </View>

        <View style={{flexDirection: 'row'}}>
        <Text style={[styles.gelenBakiyeText, { marginTop: 5 }]}>+4500,00 TL</Text>
        <TouchableOpacity onPress={() => this.faturaAlert() }>
        <Icon name="file-invoice" size={25} color={'#9f9f9f'} style={{paddingLeft : 5}}/>
        </TouchableOpacity>
        </View>
        </View>



        <View style={styles.infoView}>
        <View style={styles.infovelogo}>
        <Image  resizeMode='contain' source={require('../image/icon.png' )}style={{width: wp('15%'), height : wp('15%'),  }}  />
        
        <View style={styles.textView}>
        <Text style={styles.tarihText}>05 Nisan 2019</Text>
        <Text style={styles.akaryakitText}>Akaryakıt Yüklemesi</Text>
        </View>
        </View>

        <Text style={styles.gelenBakiyeText}>+127,00 TL</Text>
        </View>


        </ScrollView>
      </View>
    );
  }
}
const styles = {
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor : '#fff'
    },
    hesapDurumu : {
       width : wp ('100%'),
       height : hp ('11%'),
       backgroundColor : 'white',
       flexDirection: 'row',
       alignItems: 'center',
       justifyContent : 'space-between',
       borderBottomWidth : 0.4,
       paddingRight : 10,
       paddingLeft : 10,
    },
    hesapDurumuTarihView : {

    },
    buHaftaView : {
        flexDirection: 'row',
    },
    bakiye : {
        width : wp ('100%'),
        height : hp ('10%'),
        backgroundColor : 'white',
        justifyContent: 'center',
        flexDirection: 'row',
        justifyContent : 'space-between',
        alignItems: 'center',
        paddingRight : 10,
        paddingLeft : 10,
    },
    bakiyeText : {

    },
    cizgi1 : {
        width : wp ('45%'),
        height : hp ('0.5%'),
        backgroundColor: "#054b3b"
    },
    cizgi2 : {
        width : wp ('45%'),
        height : hp ('0.5%'),
        backgroundColor: "#fcc002"
    },
    sonHareketlerView : {
        width : wp ('100%'),
        height : hp ('4%'),
        backgroundColor: "#054f3e",
        alignItems: 'center',
        justifyContent : 'center'
    },
    infoView : {
        width : wp ('100%'),
        height : hp ('11%'),
        backgroundColor: "#fff",
        borderWidth : 0.4,
        borderColor : 'gray',
        justifyContent : 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight : 10,
        paddingLeft : 10,
    },
    textView : {
        marginLeft : 10
    },
    infovelogo : {
        flexDirection: 'row',
        alignItems: 'center',
    },
    hesapDurumuText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 15,
        textAlign: "left",
        color: "#000000"
    },
    tarihText  :{
        fontFamily: "SFProDisplay-Regular",
        fontSize: 13,
        textAlign: "left",
        color: "#9f9f9f"
    },
    bakiyeText : {
        fontFamily: "SFProDisplay-Bold",
        fontSize: 15,
        textAlign: "left",
        color: "#000000"
    },
    gelenBakiyeText : {
        fontFamily: "SFProDisplay-Regular",
        fontSize: 13,
        textAlign: "left",
        color: "#9f9f9f"
    },
    gidenBakiyeText : {
        fontFamily: "SFProDisplay-Regular",
        fontSize: 13,
        textAlign: "left",
        color: "#9f9f9f",
        marginLeft : 98
    },
    bakiyeText2 : {
        fontFamily: "SFProDisplay-Bold",
        fontSize: 15,
        textAlign: "left",
        color: "#000000",
        marginLeft : 88
    },
    akaryakitText : {
        fontFamily: "SFProDisplay-Regular",
        fontSize: 15,
        textAlign: "left",
        color: "#000000"
    },
    sonHareketlerText : {
        fontFamily: "SFProDisplay-Regular",
        fontSize: 15,
        textAlign: "center",
        color: "#ffffff"
    },

}
