import React, { Component } from 'react';
import { View, StatusBar, Image, ActivityIndicator, Alert } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import Api from '../components/Api';
import Store from '../mobx/Store';

export default class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
    this.accountCek();
  }

  accountCek(){
    Api.GET('api/account/2').then((res) => {
        if(res.value[0]){
            console.log(res.value[0]);
            Store.setAccount(res.value[0]);
            this.props.navigation.dispatch({type: 'Navigation/RESET', index: 0, actions: [{ type: 'Navigate', routeName:'Anasayfa'}]});
        }else{
            Alert.alert('Hata',res.message);
        }
    }).catch((err) => {
        Alert.alert('Hata','Bir hata oluştu!'+err);
    });
  }



  render() {
    return (
      <LinearGradient colors={['rgb(0,103,72)','rgb(9,129,93)', 'rgb(34,139,71)']} style={styles.container}>
      <StatusBar backgroundColor="white" barStyle="light-content" />
      <View style={{    justifyContent: 'space-between', height: hp('50%')}}>
      <Image resizeMode={'contain'} source={require('../image/logo.png')} style={styles.logo} />
      <ActivityIndicator size="large" color="#fff" />
      </View>
      </LinearGradient>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: wp('50%'),
    height: wp('50%')
  }
}