import React, { Component } from 'react';
import { View, Text, Alert } from 'react-native';
import PDFView from 'react-native-view-pdf';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const resources = {
    url: 'https://hypersoft.com.tr/zirve2.pdf',
  };

  
export default class EFatura extends Component {
  constructor(props) {
    super(props);
    this.state = {
        step: 'Veriler alınıyor.',
        stepFalse: false,
    };
  }
  
  componentWillMount(){
  var that = this;
  setTimeout(function(){ that.setState({step: that.props.yazilim+' entegresi için excel dosyası oluşturuluyor..'})}, 2000);
  setTimeout(function(){
      that.setState({ stepFalse: true });
      Alert.alert('Bilgi', that.props.yazilim+' entegresi için excel dosyası mailinize gönderilmiştir.');
    }, 4000);
  }

  render() {
    const resourceType = 'url';
    return (
      <View style={styles.container}>
        {!this.state.stepFalse ?
        <Text style={styles.duyuruText}>{this.state.step}</Text>
        :
        <PDFView
          fadeInDuration={250.0}
          style={{ height: hp('80%'), width: wp('100%'), }}
          resource={resources[resourceType]}
          resourceType={resourceType}
        />
        }
      </View>
    );
  }
}

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    duyuruText: {
        fontFamily: "SFProDisplay-Light",
        fontSize: 25,
        textAlign: "left",
        color: "#000",
        textAlign: 'center',
    }

}
