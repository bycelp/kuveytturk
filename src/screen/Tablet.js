import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView from 'react-native-maps';

export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>

      <Image source={require('../image/kuveytturk.png' )}style={{width: wp('100%'), height : wp('8%'), }}  />
     
      <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
      <View style={styles.ana}>
      <View style={styles.profilePicture}>
      <View style={styles.profileveisim}>
      <Image  source={require('../image/pp.png' )}style={{width: wp('12%'), height : wp('12%'),  borderRadius : wp('12%')/2, borderWidth : 2.5, borderColor : '#054f3e' }}  />
      

      <View style={styles.yaziView}>
      <Text style={styles.isimText}>Furkan Gökçen (Hypersoft Ticaret LTD. ŞTİ.)</Text>
      <Text style={styles.musteriText}>Müşteri No: 170550001</Text>
      </View>
      </View>

      <View style={styles.aramaView}>
      <TouchableOpacity>
      <Icon name="map-marker" size={30} color={'black'} style={{ marginRight : 35 }} />
      </TouchableOpacity>
      <TouchableOpacity>
      <Icon name="phone" size={30} color={'black'} style={{ marginRight : 35 }} />
      </TouchableOpacity>
      <TouchableOpacity>
      <Icon name="comments" size={30} color={'black'} style={{alignItems: 'center',}} />
      </TouchableOpacity>
      </View>
      </View>

      <View style={styles.kullaniciDetay}>
      <Text style={styles.baslik}>Kullanıcı Detayı</Text>
      <Text style={styles.detay}>Makine Mühendisi, 28 yaşında, Kurumsal, Kobi Müşteri</Text>
      </View>


      <View style={styles.kullaniciDetay}>
      <Text style={styles.baslik}>Talep Detayı</Text>
      <Text style={styles.detay}>XXXX işlemini yaptırtmak istiyorum, saat 15:00’a kadar belirttiğim adreste bana ulaşabilirsiniz.</Text>
      </View>


      <View style={styles.butonlar}>
      
      <TouchableOpacity style={styles.kabulEt} onPress={()=>this.props.navigation.navigate('MusteriDestek')}>>
      <Text style={styles.kabuletText}>KABUL ET</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.reddet}>
      <Text style={styles.kabuletText}>REDDET</Text>
      </TouchableOpacity>
      
      
      </View>

      <MapView style={styles.map}
    initialRegion={{
      latitude: 40.8601054,
      longitude: 29.3785741,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }}
  />
    </View>
      </ScrollView>
      
      </View>
    );
  }
}
const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor : '#ffff'

  },
  profilePicture : {
    width : wp ('95%'),
    justifyContent : 'space-between',
    alignItems: 'center',
    marginTop : 10,
    flexDirection: 'row',
  },
  yaziView : {
    justifyContent : 'center',
    marginLeft: 15

  },
  isimText : {
    fontFamily: "SFProDisplay-Light",
    fontSize: 20,
    textAlign: "left",
    color: "#000" 
  },
  musteriText : {
    fontFamily: "SFProDisplay-Light",
    fontSize: 13,
    letterSpacing: 0.59,
    textAlign: "left",
    color: "#000"
  },
  aramaView : {
    flexDirection: 'row',
},
profileveisim : {
  flexDirection: 'row',
},
kullaniciDetay : {
  width : wp('95%'),
  marginTop : 15
},
baslik : {
  fontFamily: "SFProDisplay-Regular",
  fontSize: 15,
  textAlign: "left",
  color: "#000" 
},
detay : {
  fontFamily: "SFProDisplay-Light",
  fontSize: 13,
  letterSpacing: 0.59,
  textAlign: "left",
  color: "#000",
  marginTop : 5
},
butonlar : {
  width : wp('85%'),
  flexDirection: 'row',
  justifyContent : 'space-between',
  marginTop : 15
},
kabulEt : {
  marginTop : 15,
  width : wp('40%'),
  height : hp('15%'),
  backgroundColor : 'green',
  alignItems: 'center',
  justifyContent : 'center'
},
reddet : {
  marginTop : 15,
  width : wp('40%'),
  height : hp('15%'),
  backgroundColor : 'red',
  alignItems: 'center',
  justifyContent : 'center'
},
kabuletText : {
  fontFamily: "SFProDisplay-Bold",
  fontSize: 15,
  color: "#fff" 
},
kabuletText : {
  fontFamily: "SFProDisplay-Bold",
  fontSize: 15,
  color: "#fff" 
},
map : {
  width : wp ('85%'),
  height : hp ('60%'),
  marginTop : 25,
},
ana : {
  alignItems: 'center',
},
}