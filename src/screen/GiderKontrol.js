import React, { Component } from 'react';
import { View, Text, TextInput, Image, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
    
        <View style={styles.searchbarview}>
        <View style={{alignItems:'center'}}>
        <TextInput placeholder='Ara' placeholderTextColor='white' style={styles.search}/> 
        </View>
        <Icon name="search" size={15} color={'white'} style={{position:'absolute', paddingLeft : 20}} />
        </View>

        <View style={styles.harcamaVeText}>
        
        <TouchableOpacity style={styles.harcamalar}>
        <Text style={styles.harcamaText}>HARCAMALAR</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.personeller} onPress={()=>this.props.navigation.navigate('Personeller')}>
        <Text style={styles.personellerText}>PERSONELLER</Text>
        </TouchableOpacity>

        </View>


        <View style={styles.infoView}>
        <View style={styles.infovelogo}>
        <Image  resizeMode='contain' source={require('../image/icon.png' )}style={{width: wp('15%'), height : wp('15%'),  }}  />
        
        <View style={styles.textView}>
        <Text style={styles.tarihText}>8 Nisan 2019</Text>
        <Text style={styles.burgerText}>Shell Benzin İstasyonu</Text>
        <Text style={styles.tarihText}>Eray ER</Text>
        </View>
        </View>

        <View>
        <Text style={styles.gelenBakiyeText}>230 TL</Text>
        <Text style={styles.saatText}>16.35</Text>
        </View>
        </View>


        <View style={styles.infoView}>
        <View style={styles.infovelogo}>
        <Image  resizeMode='contain' source={require('../image/eat.png' )}style={{width: wp('15%'), height : wp('15%'),  }}  />
        
        <View style={styles.textView}>
        <Text style={styles.tarihText}>12 Nisan 2019</Text>
        <Text style={styles.burgerText}>Burger King</Text>
        <Text style={styles.tarihText}>Kaan Mert Kıvrak</Text>
        </View>
        </View>

        <View>
        <Text style={styles.gelenBakiyeText}>20 TL</Text>
        <Text style={styles.saatText}>19.35</Text>
        </View>
        </View>


        <View style={styles.infoView}>
        <View style={styles.infovelogo}>
        <Image  resizeMode='contain' source={require('../image/starbucks.png' )}style={{width: wp('15%'), height : wp('15%'),  }}  />
        
        <View style={styles.textView}>
        <Text style={styles.tarihText}>13 Nisan 2019</Text>
        <Text style={styles.burgerText}>Starbucks Coffee</Text>
        <Text style={styles.tarihText}>Furkan Gökçen</Text>
        </View>
        </View>

        <View>
        <Text style={styles.gelenBakiyeText}>15 TL</Text>
        <Text style={styles.saatText}>10.35</Text>
        </View>
        </View>

        <View style={styles.altbar}>
        <View style={styles.toplamGider}>
        <Text style={styles.giderText}>Toplam Gider</Text>
        <Text style={styles.tarihText}>05-12 Nisan 2019</Text>
        </View>

        <View style={styles.fiyat}>
        <Text style={styles.fiyatText}>265 TL</Text>
        </View>
        </View>


        <View style={{paddingTop : 70}}>
        <TouchableOpacity style={styles.button} onPress={()=>this.props.navigation.navigate('Popup2')}> 
        <Text style={styles.disaaktarText}>Dışarı Aktar</Text>
        </TouchableOpacity>
        </View>





      </View>
    );
  }
}
const styles = {
    container: {
      flex: 1,
      alignItems: 'center',

    },
    searchbarview : {
        width : wp('100%'),
        height : wp('15%'),
        backgroundColor : '#054f3e',
        justifyContent : 'center',
    },
    search : {
        width : wp('95%'),
        height : wp('10%'),
        borderRadius: 11.1,
        backgroundColor: "rgba(0,0,0,0.1)",
        paddingLeft: 30,
        color : 'white'
    },
    infoView : {
        width : wp ('100%'),
        height : hp ('13%'),
        backgroundColor: "#fff",
        borderBottomWidth : 0.3,
        borderColor : 'gray',
        justifyContent : 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight : 10,
        paddingLeft : 10,
    },
    textView : {
        marginLeft : 10
    },
    infovelogo : {
        flexDirection: 'row',
        alignItems: 'center',
    },
    tarihText  :{
        fontFamily: "SFProDisplay-Regular",
        fontSize: 13,
        textAlign: "left",
        color: "#9f9f9f"
    },
    burgerText : {
        fontFamily: "SFProDisplay-Bold",
        fontSize: 17,
        textAlign: "left",
        color: "#000000"
    },
    gelenBakiyeText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 17,
        textAlign: "left",
        color: "#000"
    },
    saatText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 12,
        textAlign: "center",
        color: "#9f9f9f "
    },
    harcamalar : {
        width : wp ('50%'),
        height : hp ('7%'),
        backgroundColor: "#ffffff",
        alignItems : 'center',
        justifyContent :  'center',
        borderWidth : 0.3,
        borderColor : 'gray'
    },
    personeller : {
        width : wp ('50%'),
        height : hp ('7%'),
        backgroundColor: "#054f3e",
        alignItems : 'center',
        justifyContent :  'center',
        borderWidth : 0.3,
        borderColor : 'gray'
    },
    harcamaText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 15,
        textAlign: "center",
        color: "#027050"
    },
    personellerText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 15,
        textAlign: "center",
        color: "#ffff"

    },
    harcamaVeText : {
        flexDirection: 'row',
    },
    altbar : {
        flexDirection: 'row',
        marginTop : 15
    },
    toplamGider : {
        width : wp ('60%'),
        height : hp ('7%'),
        backgroundColor: "#ffffff",
        alignItems: 'center',
        justifyContent : 'center'
    },
    fiyat : {
        width : wp ('40%'),
        height : hp ('7%'),
        backgroundColor: "#054f3e",
        alignItems: 'center',
        justifyContent : 'center'
    },
    giderText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 17,
        textAlign: "left",
        color: "#000000"
    },
    fiyatText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 17,
        textAlign: "left",
        color: "#fff"
    },
    button : {
        width : wp ('95%'),
        height : hp ('9%'),
        backgroundColor: "#054f3e",
        alignItems: 'center',
        justifyContent : 'center',
        borderRadius : 8,
    },
    disaaktarText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 20,
        textAlign: "left",
        color: "#fff"
    }

}
