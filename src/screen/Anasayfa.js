import React, { Component } from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import Store from '../mobx/Store';
import {observer} from 'mobx-react';

@observer
export default class Anasayfa extends Component {
  render() {
    return (
      <LinearGradient colors={['rgb(0,103,72)','rgb(9,129,93)', 'rgb(34,139,71)']} style={styles.container}>
      <View style={styles.container}>

      <View style={styles.ustBar}>
      <View style={styles.profileveyazi}>
      <View style={styles.profilePicture}>
      <Image  source={require('../image/pp2.jpeg' )}style={{width: wp('20%'), height : wp('20%'),  borderRadius : wp('20%')/2, borderWidth : 2.5, borderColor : 'white' }}  />
      
      <View style={styles.yaziView}>
      <Text style={styles.isimText}>{Store.Account.customerName}</Text>
      <Text style={styles.musteriText}>Müşteri No: {Store.Account.suffix}</Text>
      <Text style={styles.musteriText}>{Store.Account.branchName}</Text>
      </View>
      </View>

      <Icon name="bell" size={25} color={'white'} style={{paddingRight : 20}}/>

      </View>
      </View>

      <View style={styles.merhabaView}>
      <Text style={styles.merhabaText}>Merhaba {Store.Account.customerName.split(" ")[0]} </Text>
      <Text style={styles.yardimText}>Nasıl yardımcı olabilirim?</Text>
      </View>

      <View style={{width: wp('70%'), height : wp('25%')}}  />

      <View style={styles.altBar}>
      <TouchableOpacity style={styles.daire} onPress={()=>this.props.navigation.navigate('Fatura')}>
      <Icon name="file" size={25} color={'white'} />
      </TouchableOpacity>

      <TouchableOpacity onPress={() => this.props.navigation.navigate('MusteriDestek')}>
      <View style={styles.mikrofon}>
      <Image  resizeMode='contain' source={require('../image/MicBtn.png' )}style={{width: wp('70%'), height : wp('30%'),  }}  />
      </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.daire} onPress={() => this.props.navigation.navigate('GiderKontrol')}>
      <Icon name="search" size={25} color={'white'} />
      </TouchableOpacity>
      </View>

      </View>
      </LinearGradient>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',

  },
  ustBar : {
    width : wp('100%'),
    height : hp('25%'),
    justifyContent : 'center'

  },
  profilePicture : {
    marginLeft : 10,
    justifyContent : 'center',
    flexDirection: 'row',
  },
  yaziView : {
    justifyContent : 'center',
    marginLeft : 10
  },
  profileveyazi : {
    flexDirection : 'row',
    justifyContent : 'space-between',
    alignItems : 'center'
  },
  isimText : {
    fontFamily: "SFProDisplay-Light",
    fontSize: 25,
    textAlign: "left",
    color: "#ffff" 
  },
  musteriText : {
    fontFamily: "SFProDisplay-Light",
    fontSize: 15,
    letterSpacing: 0.59,
    textAlign: "left",
    color: "rgba(255, 255, 255, 0.5)"
  },
  merhabaText : {
    fontFamily: "SFProDisplay-Light",
    fontSize: 35,
    textAlign: "left",
    color: "#ffff" 
  },
  yardimText : {
    fontFamily: "SFProDisplay-Light",
    fontSize: 25,
    letterSpacing: 0.59,
    textAlign: "left",
    color: "rgba(0, 0, 0, 0.5)"
  },
  merhabaView : {
    width : wp('90%'),
    height : hp('25%'),
    justifyContent : 'center',
  },
  altBar : {
    width : wp('93%'),
    height : hp('15%'),
    marginTop : 80,
    flexDirection: 'row',
    justifyContent : 'space-between',
    alignItems : 'center'
  },
  daire : {
    width : wp ('15%'),
    height : wp ('15%'),
    borderRadius : wp ('15%')/2,
    backgroundColor: "#024533",
    alignItems : 'center',
    justifyContent : 'center'
  },
  mikrofon : {
    width : wp ('25%'),
    height : wp ('25%'),
    borderRadius : wp ('25%')/2,
    backgroundColor: "white",
    borderStyle: "solid",
    borderWidth: 6,
    borderColor: "gray",
    alignItems : 'center',
    justifyContent : 'center'  
  }

});