import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.popup}>
        <Text style={{ fontSize: 18, textAlign: 'center', color: '#fff' }}>Verileri dışarı aktarmak istediğiniz platformu seçiniz.</Text>
        </View>
        <View style={styles.popup2}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Popup', { yazilim: 'Zirve' } )}>
        <Image  resizeMode='contain' source={require('../image/zirve.png' )}style={{width: wp('60%'), height : wp('20%'),  }}  />
        </TouchableOpacity>
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Popup', { yazilim: 'Luca' } )}>
        <Image  resizeMode='contain' source={require('../image/luca.png' )}style={{width: wp('60%'), height : wp('20%'),  }}  />
        </TouchableOpacity>


        <TouchableOpacity onPress={() => this.props.navigation.navigate('Popup', { yazilim: 'Excel' } )}>
        <Image  resizeMode='contain' source={require('../image/excel.png' )}style={{width: wp('60%'), height : wp('20%'),  }}  />
        </TouchableOpacity>


        </View>
      </View>
    );
  }
}
const styles = {
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent : 'center'

    },
    popup : {
       width : wp('90%'),
       height : hp('15%'),
       backgroundColor : '#054f3e',
       justifyContent : 'center',
       textAlign : 'center',
       borderRadius : 15,

    },
    popup2 : {
        width : wp('90%'),
        height : hp('40%'),
        backgroundColor : '#fff',
        marginTop : -10,
        borderBottomRightRadius : 15,
        borderBottomLeftRadius : 15,
        alignItems: 'center',
        justifyContent : 'center'
    },
    yuvarlak : {
        width : wp('80%'),
        height : hp('20%'),
        borderColor : 'black'
    },
}
