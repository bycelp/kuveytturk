import React, { Component } from 'react';
import { View, Text, Image, TextInput , TouchableOpacity, Alert} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';
import Api from '../components/Api';
import Store from '../mobx/Store';

export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      personel1: false,
      personel2: false,
      personel3: false,
    };
  }

  limitTanimla(){
    Api.POST('api/limitUpdate', { cardNumber: '1234567890123456', limit: 5000.0}).then((res) => {
        if(res.status == 'SUCCESS'){
            Alert.alert('Bilgi',res.message);
        }else{
            Alert.alert('Hata',res.message);
        }
    }).catch((err) => {
        Alert.alert('Hata','Bir hata oluştu!'+err);
    });
}

  step(){
    return(
      <View>
      <View style={styles.odemeanaview}>
      <View style={styles.sepettutari}>
      <Text style={styles.odemetext}>Personel Maaşı</Text>
      <View style={{flexDirection: 'row'}}>
      <TextInput keyboardType={'number-pad'} value={'4500'} style={styles.textInput} />
      <Text style={styles.fiyat}>TL</Text>
      </View>
      </View>

      <View style={styles.kargoucreti}>
      <Text style={styles.odemetext}>Akaryakıt Bütçesi</Text>
      <View style={{flexDirection: 'row'}}>
      <TextInput keyboardType={'number-pad'} value={'300'} style={styles.textInput} />
      <Text style={styles.fiyat}>TL</Text>
      </View>
      </View>

      <View style={styles.odenecektutar}>
      <Text style={styles.odemetext}>Yemek Bütçesi</Text>

      <View style={{flexDirection: 'row'}}>
      <TextInput keyboardType={'number-pad'} value={'200'} style={styles.textInput} />
      <Text style={styles.fiyat}>TL</Text>
      </View>
      </View>


      </View>

      <View style={styles.hizala}>
      
      <View style={styles.hareketler}>
      <Text style={styles.harcamaText}>Hareketler</Text>
      </View>

      <TouchableOpacity onPress={() => this.limitTanimla()} style={styles.hareketler}>
      <Text style={styles.harcamaText}>Kaydet</Text>
      </TouchableOpacity>
      
      </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.searchbarview}>
        <View style={{alignItems:'center'}}>
        <TextInput placeholder='Ara' placeholderTextColor='white' style={styles.search}/> 
        </View>
        <Icon name="search" size={15} color={'white'} style={{position:'absolute', paddingLeft : 20}} />
        </View>

        <View style={styles.harcamaVeText}>
        <TouchableOpacity style={styles.harcamalar} onPress={()=>this.props.navigation.navigate('GiderKontrol')}>>
        <Text style={styles.harcamaText}>HARCAMALAR</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.personeller} onPress={()=>this.props.navigation.navigate('Personeller')}>
        <Text style={styles.personellerText}>PERSONELLER</Text>
        </TouchableOpacity>
        </View>


        <TouchableOpacity onPress={() => this.setState({ personel1: !this.state.personel1 })} style={styles.ilanbaslik}>
        <View style={styles.logoveyazilar}>
        <View style={styles.ilanlogo}>
        <Image  resizeMode='contain' source={require('../image/pp.png' )}style={{width: wp('15%'), height : wp('15%'), borderRadius : wp('15%')/2 }}  />
        </View> 

        <View style={styles.ilanyazilari}>
        <Text style={styles.basliktext}>Eray ER</Text>
        <Text style={styles.detaytext}>Yazılım Geliştirmeci</Text>
        </View>
        </View>

        <Icon name="chevron-right" size={20} color={'#00694a'}  />

        </TouchableOpacity>

        {this.state.personel1 && this.step()}


        <TouchableOpacity onPress={() => this.setState({ personel2: !this.state.personel2 })} style={styles.ilanbaslik}>
        <View style={styles.logoveyazilar}>
        <View style={styles.ilanlogo}>
        <Image  resizeMode='contain' source={require('../image/pp.png' )}style={{width: wp('15%'), height : wp('15%'), borderRadius : wp('15%')/2 }}  />
        </View> 

        <View style={styles.ilanyazilari}>
        <Text style={styles.basliktext}>Furkan Gökçen</Text>
        <Text style={styles.detaytext}>Yazılım Geliştirmeci</Text>
        </View>
        </View>

        <Icon name="chevron-right" size={20} color={'#00694a'}  />

        </TouchableOpacity>


        {this.state.personel2 && this.step()}


        <TouchableOpacity onPress={() => this.setState({ personel3: !this.state.personel3 })} style={styles.ilanbaslik}>
        <View style={styles.logoveyazilar}>
        <View style={styles.ilanlogo}>
        <Image  resizeMode='contain' source={require('../image/pp.png' )}style={{width: wp('15%'), height : wp('15%'), borderRadius : wp('15%')/2 }}  />
        </View> 

        <View style={styles.ilanyazilari}>
        <Text style={styles.basliktext}>Kaan Yiğit Kıvrak</Text>
        <Text style={styles.detaytext}>Yazılım Geliştirmeci</Text>
        </View>
        </View>
        <Icon name="chevron-right" size={20} color={'#00694a'}  />
        </TouchableOpacity>

        {this.state.personel3 && this.step()}

      </View>
    );
  }
}
const styles = {
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: "#ffffff",
    },
    odemeanaview : {
        backgroundColor: "#ffffff",

    },
    sepettutari : {
        width : wp('90%'),
        height : hp('5%'),
        borderBottomWidth: 1,
        borderColor : '#eeeeee',
        marginLeft : 18,
        flexDirection: 'row',
        justifyContent : 'space-between',
        alignItems: 'center',
    },
    kargoucreti : {
        width : wp('90%'),
        height : hp('5%'),
        borderBottomWidth: 1,
        borderColor : '#eeeeee',
        marginLeft : 18,
        flexDirection: 'row',
        justifyContent : 'space-between',
        alignItems: 'center',
    },
    odenecektutar : {
        width : wp('90%'),
        height : hp('5%'),
        borderBottomWidth: 1,
        borderColor : '#eeeeee',
        marginLeft : 18,
        flexDirection: 'row',
        justifyContent : 'space-between',
        alignItems: 'center',
    },
    odemetext : {
        fontFamily: "SFProDisplay-Semibold",
        fontSize: 15,
        textAlign: "left",
        color: "#01694a"
    },
    fiyat : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 15,
        textAlign: "left",
        color: "gray"
    },
    ilanbaslik : {
        width : wp('100%'),
        height : wp('20%'),
        backgroundColor : '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent : 'space-between',
        padding: 15,
        borderBottomWidth: 0.3,
        borderColor : '#c1c1c1'
      },
      ilanlogo : {
        width : wp('12%'),
        height : wp('12%'),
        backgroundColor: '#87e223',
        borderRadius : 22,
        justifyContent : 'center',
        alignItems: 'center',
        marginRight : 10
      },
      basliktext : {
        fontFamily: "SFProDisplay-Semibold",
        fontSize: 20,
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "#030303",
      },
      detaytext : {
        fontFamily: "SFProDisplay-Regular",
        fontSize: 14,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "#c1c1c1"
      },
      logoveyazilar : {
        flexDirection: 'row',
      },
    ilanyazilari : {
        marginLeft : 10
    },
    searchbarview : {
        width : wp('100%'),
        height : wp('15%'),
        backgroundColor : '#054f3e',
        justifyContent : 'center',
    },
    search : {
        width : wp('95%'),
        height : wp('10%'),
        borderRadius: 11.1,
        backgroundColor: "rgba(0,0,0,0.1)",
        paddingLeft: 30,
        color : 'white'
    },
    harcamalar : {
        width : wp ('50%'),
        height : hp ('7%'),
        backgroundColor: "#054f3e",
        alignItems : 'center',
        justifyContent :  'center',
        borderWidth : 0.3,
        borderColor : 'gray'
    },
    personeller : {
        width : wp ('50%'),
        height : hp ('7%'),
        backgroundColor: "#fff",
        alignItems : 'center',
        justifyContent :  'center',
        borderWidth : 0.3,
        borderColor : 'gray'
    },
    harcamaText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 15,
        textAlign: "center",
        color: "#ffff"
    },
    personellerText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 15,
        textAlign: "center",
        color: "#054f3e"

    },
    harcamaVeText : {
        flexDirection: 'row',
    },
    hizala : {
        marginTop : 15,
        width : wp ('85%'),
        flexDirection: 'row',
        justifyContent : 'space-between',
        marginBottom : 15
    },
    hareketler : {
        width : wp ('40%'),
        height : hp ('6%'),
        borderRadius: 10,
        backgroundColor: "#054f3e",
        alignItems: 'center',
        justifyContent : 'center'
    },
    button : {
        width : wp ('95%'),
        height : hp ('9%'),
        backgroundColor: "#054f3e",
        alignItems: 'center',
        justifyContent : 'center',
        borderRadius : 8,
    },
    disaaktarText : {
        fontFamily: "SFProDisplay-Medium",
        fontSize: 20,
        textAlign: "left",
        color: "#fff"
    },
    textInput: {
      width: wp('40%'),
      textAlign: 'right',
      marginRight: 5,
      fontFamily: "SFProDisplay-Medium",
      fontSize: 15,
      color: "gray",
    }

}