import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView,{ Marker } from 'react-native-maps';


export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>


      <MapView style={styles.map}
      initialRegion={{
      latitude: 40.8601054,
      longitude: 29.3785741,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
      }}
      >
      <Marker
      coordinate={{
        latitude: 40.8601054,
        longitude: 29.3785741,
      }}
      title={'Banka Temsilcisi'}
      description={'Size 5 dk uzaklıkta'}
      showsUserLocation={true}
    />

      </MapView>
        <View style={{position:'absolute'}}>

        <View style={{alignItems : 'center'}}>
        <TouchableOpacity style={styles.button} onPress={() => {
          Alert.alert('Bilgi', 'Çağrınız iletildi. 5 dakika içerisinde müşteri temsilcimiz size ulaşacak.');
          this.props.navigation.pop();
        }}> 
        <Text style={styles.disaaktarText}>En Yakın Temsilci Çağır</Text>
        </TouchableOpacity>
        </View>
      </View>

      </View>
    );
  }
}
const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor : '#ffff',
    justifyContent : 'flex-end'

  },
  ustbar : {
    width : wp('100%'),
    height : hp('10%'),
    backgroundColor : 'green',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  map : {
    width : wp ('100%'),
    height : hp ('90%'),

  },
  button : {
    width : wp ('95%'),
    height : hp ('9%'),
    backgroundColor: "#054f3e",
    alignItems: 'center',
    justifyContent : 'center',
    borderRadius : 8,
    marginBottom : 15
},
disaaktarText : {
  fontFamily: "SFProDisplay-Medium",
  fontSize: 20,
  textAlign: "left",
  color: "#fff"
}

}
