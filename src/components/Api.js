
import {AsyncStorage} from 'react-native';

var server = "https://api-test.hypersoft.com.tr";
var host   = "api-test.hypersoft.com.tr";

var Api = {

 POST: async function(params1,veriler) {
    const token = await AsyncStorage.getItem('access_token');
    var r = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Origin': '',
        'Host': host,
        'Authorization': token
      },
      body: JSON.stringify(veriler)
    };
    return fetch(server+'/'+params1, r)
    .then((response) => response.json())
 },

 GET: async function(params1) {
  const token = await AsyncStorage.getItem('access_token');
  var r = {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Origin': '',
      'Host': host,
      'Authorization': token
    },
  };
  return fetch(server+'/'+params1, r)
  .then((response) => response.json())
},

GETURL: async function(url) {
  var r = {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Origin': '',
      'User-Agent': '',
    },
  };
  return fetch(url, r)
  .then((response) => response.json())
},

}

module.exports = Api;