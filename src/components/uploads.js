import RNFetchBlob from 'rn-fetch-blob'

let upload = (data,islem,token) => {
  return RNFetchBlob.fetch('POST', 'https://api-test.hypersoft.com.tr/'+islem, {
    otherHeader : "foo",
    'Content-Type' : 'multipart/form-data',
    'Authorization': token
  }, data);
}

module.exports = upload;